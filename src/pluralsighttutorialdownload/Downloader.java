/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pluralsighttutorialdownload;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 *
 * @author apl
 */
public class Downloader {

    static int downloadTryCount = 0;

    //http://vid.pluralsight.com/expiretime=1493545269/b2501c0471767509afa390a343f6cd8e/clip-videos/james-toto/elasticsearch-for-dotnet-developers-m1/elasticsearch-for-dotnet-developers-m1-01/1024x768mp4/20151117141347.mp4
    public static void downloadVideoFromUrl(String url, String directory, String videoTitle) {
        downloadTryCount = 0;
        downloadUsingNIO(url, directory + "/" + videoTitle + ".mp4");

    }

    private static void downloadUsingNIO(String urlStr, String file) {
        try {
            downloadTryCount++;
            if (downloadTryCount > 5) {
                PluralSightTutorial.addToDisplayLog("" + file + " : " + " Failed to Download...!");
                return;
            }
            if (isFileExist(file)) {
                PluralSightTutorial.addToDisplayLog("" + file + " ---- Successfully Downloaded.");
                PluralSightTutorial.downloadedVideoCount += 1;
                return;
            }
            PluralSightTutorial.addToDisplayLog("Started Downloading : " + file + "");
            URL url = new URL(urlStr);
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            FileOutputStream fos = new FileOutputStream(file);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            rbc.close();
            PluralSightTutorial.downloadedVideoCount += 1;
        } catch (IOException ex) {
            Toolkit.getDefaultToolkit().beep();
            PluralSightTutorial.displayException(ex);
            PluralSightTutorial.addToDisplayLog(file + " : " + "Download will Statr Again.... ");
            downloadUsingNIO(urlStr, file);
        }
    }

    public static boolean isFileExist(String filePath) {
        File f = new File(filePath);
        return f.exists() && !f.isDirectory();
    }
}
